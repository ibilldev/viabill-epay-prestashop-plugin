### PRESTASHOP : ViaBill ePay Module 
=====================
A module to be integrated in Prestashop WebShop for providing Payment Option and not a Payment Gateway.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
-----
- version: 1.0
- Module on BitBucket(https://bitbucket.org/ibilldev/viabill-epay-prestashop-plugin/)


###Description###
-----------
Pay using ViaBill. 
Install this Module in to your Prestashop Web Shop to provide an option to pay using ViaBill through ePay Payment Gateway.

#Requirements
------------
* PHP >= 5.2.0
* 
#Compatibility
-------------
* Prestashop >= 1.4

###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 
2. Module contains one folders a.) modules/viabillepay

Figure shows the folder structure to place the necessary files for the module.

Prestashop
 |
 +-- modules
 |  |  
 |  +-- viabillepay
 |  |      
 
 |

3. Within the 'modules' folder, place the folder named 'viabillepay'.

4. Go to the Admin-> Modules and Services-> Modules and Services

5. In the Modules List->Payments and Gateways, Filter : 'Installed and Not Installed'  | Enabled and Disabled Modules Author : viabill epay

6. Click on Configure for ViaBill.

7. Enter ePay Merchant Number.

8. Enable Payment Request : Yes

9. Save and Done.


##Uninstallation/Disable Module/Delete Module
-----------------------
1. Go to the Admin->Modules and Services-> Modules and Services
2. Modules List->Payment and Gateways, Filter : 'Installed'  | Enabled Modules Author : viabill epay
3. Choose Option to Uninstall/Disable/Delete the module.



#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

#Copyright
---------
(c) 2015 ViaBill
