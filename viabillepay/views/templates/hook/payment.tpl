
<form action="https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx" method="post" id="ViaBillPayForm">
	{foreach from=$parameters key=k item=v}
		{if ($k|replace:'viabill_epay_':'') == "windowstate"}
			<input type="hidden" name="windowstate" value="3">
		{else}
			<input type="hidden" name="{$k|replace:'viabill_epay_':''}" value="{$v}">
		{/if}
	{/foreach}
</form>


<p class="payment_module" >
	<div style="">
	<a title="{l s='Pay using ViaBill Epay' mod='viabillepay'}" href="javascript: ViaBillPayForm.submit();">
		
				<img src="{$this_path_viabillepay}viabill.png" alt="{l s='Pay using Viabill ePay' mod='viabillepay'}" style="float:left; margin-right: 15px;margin-top: -19px" />
				<div id="defaulttext"><div style="line-height:0px"  id="vbpricetag" class="viabill-pricetag" view="payment" price="{$pricetag}"> {l s='ViaBill køb nu - betal, når du vil'   mod='viabillepay'}
				</div></div>
	</a>
</div>
</p>


{if $showpricetag == 'TRUE'}

<script type="text/javascript">
 {$vbpricetagsrc}
setTimeout(function(){	

	var priceqp = {$pricetag};				
	if(vb.isLow(priceqp)  || vb.isHigh(priceqp)){		
			//$('#vbpricetag').addClass('viabill-pricetag');
	}
	else{		
		$('#defaulttext').html('<div id="defaulttext"><div style="line-height:0px"  id="vbpricetag"  price="{$pricetag}"> {l s="ViaBill køb nu - betal, når du vil"  mod="viabillepay"}');
	}
}, 1000);
</script>

{/if}

