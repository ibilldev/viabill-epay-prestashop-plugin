<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{viabillepay}default>viabillepay_31211b6813083b13df997b9e3abbbd2b'] = 'Lieferung';
$_MODULE['<{viabillepay}default>viabillepay_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Lieferung';
$_MODULE['<{viabillepay}default>viabillepay_878c9cfae6779e0c60568a94b4c77a57'] = 'Bezahlung abgewickelt';
$_MODULE['<{viabillepay}default>viabillepay_7a68d0e53be91b1dff60c7e1adc0aff0'] = 'Bezahlung kreditiert';
$_MODULE['<{viabillepay}default>viabillepay_2865c62239a8a439963b4e3d42fa1b25'] = 'Bezahlung gelöscht';
$_MODULE['<{viabillepay}default>viabillepay_7319aa25b8e2d98243fa2a8ea23a4dcd'] = 'Bezahlung abgeschlossen';
$_MODULE['<{viabillepay}default>viabillepay_0f91ee0941e61fb068e4583673323324'] = 'Verdächtige Bezahlung';
$_MODULE['<{viabillepay}default>viabillepay_44a50f07b4bdc57740901280f9eddaf5'] = 'Abheben';
$_MODULE['<{viabillepay}default>viabillepay_f2a6c498fb90ee345d997f888fce3b18'] = 'Löschen';
$_MODULE['<{viabillepay}default>viabillepay_6f4e69b8c12697659a0555393fb95cfb'] = 'Möchten Sie wirklich löschen?';
$_MODULE['<{viabillepay}default>viabillepay_34f5759d35725fea3a4e7396146fc17d'] = 'Transaktion abschließen';
$_MODULE['<{viabillepay}default>viabillepay_0a90b1bc4078f74b6f0d117ec7df65af'] = 'Kreditieren';
$_MODULE['<{viabillepay}default>viabillepay_e80592d6eed1cd99117bbf627f702db1'] = 'Möchten Sie kreditieren:';
$_MODULE['<{viabillepay}default>viabillepay_5fe6005bf6e415c950c011fb65f12b8f'] = 'Gelöscht';
$_MODULE['<{viabillepay}default>viabillepay_44749712dbec183e983dcd78a7736c41'] = 'Datum';
$_MODULE['<{viabillepay}default>viabillepay_a4ecfc70574394990cf17bd83df499f7'] = 'Handlung';
$_MODULE['<{viabillepay}default>payment_d2792b17b0ebab214d6a1ca38b362906'] = 'Mit ViaBill ePay bezahlen';
